/*
 * Copyright (c) 2013 Indian Institute of Technology, Madras. 
 * All Rights Reserved.

 * This program is free software. You can redistribute and/or modify
 * it in accordance with the terms of the accompanying license
 * agreement.  See LICENSE in the top-level directory for details.
 */

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/stat.h>

#include "readwholefile.h"

/* This routine returns the size of the file it is called with. */

static unsigned get_file_size (const char * file_name)
{
    struct stat sb;
    if (stat (file_name, & sb) != 0) {
        fprintf (stderr, "'stat' failed for '%s': %s.\n",
                 file_name, strerror (errno));
        exit (EXIT_FAILURE);
    }
    return sb.st_size;
}

/* This routine reads the entire file into memory. */


void read_whole_file (const char * file_name, unsigned char ** _contents, size_t *_bytes_read)
{
    unsigned s;

    unsigned char * contents;
    FILE * f;
    size_t bytes_read;

    int status;

    s = get_file_size (file_name);
    contents = (unsigned char*) malloc (s + 1);
    if (! contents) {
        fprintf (stderr, "Not enough memory.\n");
        exit (EXIT_FAILURE);
    }

    f = fopen (file_name, "r");
    if (! f) {
        fprintf (stderr, "Could not open '%s': %s.\n", file_name,
                 strerror (errno));
        exit (EXIT_FAILURE);
    }
    bytes_read = fread (contents, sizeof (unsigned char), s, f);
    if (bytes_read != s) {
        fprintf (stderr, "Short read of '%s': expected %d bytes "
                 "but got %d: %s.\n", file_name, s, bytes_read,
                 strerror (errno));
        exit (EXIT_FAILURE);
    }

    status = fclose (f);
    if (status != 0) {
        fprintf (stderr, "Error closing '%s': %s.\n", file_name,
                 strerror (errno));
        exit (EXIT_FAILURE);
    }
    //return contents;
    *_bytes_read = bytes_read;
    *_contents = contents;
}

char* myfgets(char *line, int buffer_size)
{
    static int pos = 0;
    
    if (pos >= bytes_read)
    {
        return NULL;
    }

    char c = 0;
    int i = 0;    
    while (pos < bytes_read && i < buffer_size - 1)
    {
        c = file_contents[pos];
        if ( c == '\n')
        {
            pos++;
            break;
        }
        else
        {
            line[i++] = c;
        }
        pos++;
        
    }
    
    line[i] = 0;
    return (char*)1;
}
