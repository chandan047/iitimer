# 
#  Copyright (c) 2013 Indian Institute of Technology, Madras. 
#  All Rights Reserved.

#  This program is free software. You can redistribute and/or modify
#  it in accordance with the terms of the accompanying license
#  agreement.  See LICENSE in the top-level directory for details.

# Find probabily that a Gaussian random variable N(mu1,std1) is greater than another
# N(mu2,std2)

# Generate samples X1, X2 of each of the random variable
# Count number of numbers Y = X1 - X2 > 0

import numpy as np
import scipy.special as sp

import matplotlib
matplotlib.use('GTKCairo')
import matplotlib.pyplot as plt
import math

def Gaussian_cdf(x,mu,std):
	# 0.5 * ( 1 + erf((x-mu)/(sqrt2*std)) )
	return 0.5*(1+sp.erf((x-mu)/(std*np.sqrt(2))))

def Gaussian_pdf(x,mu,std):
	# 1/(sqrt(2 pi) std) exp(-(x-mu)^2 / (2 std^2) )
	return (1/(np.sqrt(2*np.pi)*std))*np.exp(-pow((x-mu),2)/(2*pow(std,2)))

def plotHistogram(x,color='b'):
	hist,bins=np.histogram(x,bins=100,normed=True)
	plt.plot(0.5*(bins[:-1]+bins[1:]),hist,color+'*-')
	plt.xlabel('x')
	plt.ylabel('Normalised Frequency')

## Parameters
mu1 = 4.06900121e-12
std1 = sqrt(8.21037638e-17)
mu2 = 6.86498081e-12
std2 = sqrt(2.27075213e-16)
##


xx = np.linspace(0,2,1000)
yy1 = Gaussian_pdf(xx,mu1,std1)
yy2 = Gaussian_pdf(xx,mu2,std2)

plt.plot(xx,yy1,'k--')
plt.plot(xx,yy2,'k--')

# Verification
# Draw two random numbers s1, s2
# Define sm = max (s1, s2)
N = 100000
S1 = np.random.normal(mu1,std1,N)
S2 = np.random.normal(mu2,std2,N)
SM = []
SM_correlated = []
count = 0
for i in range(len(S1)):
	x1 = S1[i]
	x2 = S2[i]
	x2_correlated = mu2 + (x1 - mu1) * (std2/std1)

	SM.append(np.amax([x2,x1]))
	SM_correlated.append( np.amax([x1,x2_correlated]) )

SM = np.array(SM)
a = SM
one = a[np.where(a>0)[0]]
two = one[np.where(one<2)[0]]
plotHistogram(two, color='b')

SM_correlated = np.array(SM_correlated)
a = SM_correlated
one = a[np.where(a>0)[0]]
two = one[np.where(one<2)[0]]
plotHistogram(two,color='r')

plt.title('Histogram plot. Y = max(X1,X2)')
plt.grid(True)
plt.legend([r"$X1\sim N\left(1.0,0.1^{2}\right)$",r"$X1\sim N\left(1.0,0.5^{2}\right)$",r"$Y_{1}\left(\rho=0\right)$", r"$Y_{2}\left(\rho=1.0\right)$"])
plt.xlim(0,2)
plt.show()

